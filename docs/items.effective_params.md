# 道具能力參數定義表

角色成長相關能力參數
 - HP
 - MP
 - SP
 - AD 攻擊
 - SP 法傷
 - AR 物防
 - MR 魔防
 - DODGE 迴避
 - LUCK 幸運


回復相關參數
 - HP_heal
 - MP_heal
 - SP_heal

暫時 Buff 相關參數
 - HP_up
 - MP_up
 - SP_up
 - AD_up
 - AP_up
 - AR_up
 - MR_up
 - DODGE_up
 - LUCK_up


# 道具能力參數範例

### 裝備

長劍

`魔法` 長劍

`稀有` 長劍

`傳奇` 長劍


#### 消耗

紅色藥水

藍色藥水

黃色藥水

抗性藥水...(時間性)

重置技能點數卷軸

EXP提升卷軸

素材獲得率提升卷軸

