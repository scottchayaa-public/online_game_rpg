# ToDo List

 - [items.class](items.class.md) 道具類型的規範
 - [items.effective_params](items.effective_params.md) 道具能力參數定義表
 - character_items.extra_effective_params 道具額外附加能力參數
 - items.split_params 道具分解參數定義表
 - enemies.drop_items_params 怪物掉落道具參數定義表
 - enemies.ability_params 怪物能力值定義表
 - characters.ability_params 角色能力值定義表
 - 裝備強化的定義還在想, 相關表: items, character_items.refine_level
 - 角色背包容量, 道具數量限制 (目前規劃是無容量限制: character_items)


# 道具系統 - 架構圖

![](道具系統_架構.png)


# 資料結構
![](online_game_rpg.png)


# 使用工具

 - [MySQL Workbench](https://dev.mysql.com/downloads/workbench/)
   - 支援開啟副檔名 : .mwb
   - workbench 裡面有 table 欄位註解
 - [Axure RP](https://www.axure.com/)
   - 支援開啟副檔名 : .rp



